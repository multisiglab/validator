require('babel-register')
const exec = require("child_process").exec
const eos = require("../integration/eos_connector");

const contractHolder = "ivanver"
const account = "nik"

async function sh(cmd) {
  return new Promise(function (resolve, reject) {
      exec(cmd, (err, stdout, stderr) => {
          if (err) {
              reject(err);
          } else {
              resolve({ stdout, stderr });
          }
      });
  });
}

function validate() {
  eos.getTableRows({
    "json": true,
    "code": contractHolder,
    "scope": contractHolder,
    "table": "repos",
    "table_key": "uint64"
  }).then(res => {
    res.rows.forEach(row => {
      if (row.status != 1)
        return

      console.log("START VALIDATING...")
      
      row.hosts.forEach(host => {
        sh(`bash ./tools/check.sh ${host.hostlink}`).then(res => {
          let stdout = res.stdout.substring(0, res.stdout.length-1)

          console.log("Origin user repo hash in IPFS ", row.userlink)
          console.log("Calculated repo hash from remote code storage in IPFS ", stdout)

          eos.transaction({
            actions: [{
              account: contractHolder,
              name: 'validatehost',
              authorization: [{
                actor: account,
                permission: 'active'
              }],
              data: {
                reponame: row.reponame,
                hostlink: host.hostlink,
                valid: stdout == row.userlink ? 1 : 0
              }
            }]
          })
        })
      })
    })
  })
}

validate()
