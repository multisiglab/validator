const Eos = require('eosjs') // Eos = require('./src')

// Optional configuration..
const config = {
    chainId: null, // 32 byte (64 char) hex string
    keyProvider: [
        '5KHAbcCEiCNn7BRntDH9nTMNEVHDvbw8H8fgvEBxbL7mxu4SMTf',
    ], // WIF string or array of keys..
    httpEndpoint: 'http://10.101.2.38:8888',
    mockTransactions: () => null, // or 'fail'
    // transactionHeaders: (expireInSeconds, callback) => {
    //   callback(null/*error*/, headers)
    // },
    expireInSeconds: 60,
    broadcast: true,
    debug: false, // API and transactions
    sign: true
}

module.exports = Eos(config)